# Android Challenge

## Considerações Gerais

Você deverá fazer um fork deste projeto, todos os seus commits devem estar registrados nele, pois queremos ver como você trabalha.

## O desafio

Seu desafio será criar um crud usando o banco de dados do celular.


deverá ter uma tela inicial para cadastrar os seguintes dados: 

- Modelo do veículo

- Cor do veículo

- Marca do veículo

- Ano do veículo

- Placa do veículo

Usa o modelo abaixo como base: 


![](https://i1.wp.com/mobiledevhub.com/wp-content/uploads/2017/11/Screenshot_1510261467.png?resize=360%2C600)


Depois você deve criar uma lista com esses cadastro usando Recycleview.

os item da lista terá que ter a opção para editar, excluir e ver detalhes.



Observações: 

- Pode utilizar as libs que achar necessário, apenas descreva no README qual foram usadas.

- A persitência no banco deverá ser utilizando SQLite.

- A linguagem para realização do desafio é Java.

- Api mínima: 21

- Uso de material design.


## Como entregar?

* Faça um fork deste projeto e nos envie o seu link